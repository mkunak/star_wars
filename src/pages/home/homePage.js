import React from 'react';
import styles from "./homePage.module.scss";

function HomePage() {
  return (
    <div className={`${styles.pageHeight}`}>
      <h2 className={`${styles.verticalCenter} text-warning`}>
        Wellcome to Star Wars API
      </h2>
    </div>
  );
}

export default HomePage;
