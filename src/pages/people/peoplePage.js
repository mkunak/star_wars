import React from 'react';
import {withRouter} from 'react-router-dom';

import ErrorLoader from "../../components/errorLoader"; // import RandomItem from "../randomItem";
import ItemsList from "../../components/itemsList";
import ItemDetails from "../../components/itemDetails";
import RandomItem from "../../components/randomItem";
import SwapiService from "../../services/swapiService";

import styles from "./peoplePage.module.scss";


class PeoplePage extends React.Component {
  
  swapiService = new SwapiService();
  state = {
    itemId: Math.floor(Math.random() * (18 - 3)) + 3,
    hasError: false
  };
  
  itemsListArray = () => {
    let array = [];
    for (let i = 1; i <= 88; i++) {
      if (i !== 17) {
        array.push(i);
      }
    }
    return array;
  };
  
  componentDidCatch(error, errorInfo) {
    console.log('Errors have come');
    this.setState({
      hasError: true
    })
  }
  
  componentWillUnmount() {
    this.setState(() => {
      return {
        itemId: null
      };
    });
  }
  
  setSelectedId = (id) => {
    this.setState({
      itemId: id
    })
  };
  
  render() {
    
    if (this.state.hasError) {
      return <ErrorLoader/>;
    }
    
    const {showRandomItem, showItemsList, showItemDetails, match, history} = this.props;
    
    const randomItem = showRandomItem ?
      <RandomItem url={'characters/'}
                  itemsListArray={this.itemsListArray()}
                  getItem={this.swapiService.getPerson}/> : null;
    
    const itemsList = showItemsList ?
      <ItemsList url={'people/'}
        // onItemSelected={this.setSelectedId}
                 onItemSelected={(itemId) => {
                   const newPath = `/people/${itemId}`;
                   history.push(newPath);
                 }}
                 getAllItems={this.swapiService.getAllPeople}/> : null;
    
    const itemDetails = showItemDetails ?
      <ItemDetails url={'characters/'}
                   itemId={match.params.id || this.state.itemId}
                   getItemDetails={this.swapiService.getPerson}/> : null;
    
    return (
      <section className={`${styles.pageHeight} people`}>
        <div className="row my-4">
          <div className="col-12">
            {randomItem}
          </div>
          <div className="col-md-6">
            {itemsList}
          </div>
          <div className="col-md-6">
            {itemDetails}
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(PeoplePage);
