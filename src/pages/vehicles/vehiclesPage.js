import React from 'react';
import {withRouter} from 'react-router-dom';

import ErrorLoader from "../../components/errorLoader"; // import RandomItem from "../randomItem";
import ItemsList from "../../components/itemsList";
import SwapiService from "../../services/swapiService";
import ItemDetails from "../../components/itemDetails";
import RandomItem from "../../components/randomItem";

import styles from './vehiclesPage.module.scss';


class VehiclesPage extends React.Component {
  
  swapiService = new SwapiService();
  
  itemsListArray = [4, 6, 7, 8, 14, 16, 18, 19, 20, 24];
  
  state = {
    itemId: Math.floor(Math.random() * (6 - 3)) + 3,
    hasError: false
  };
  
  componentWillUnmount() {
    this.setState(() => {
      return {
        itemId: null
      };
    });
  }
  
  componentDidCatch(error, errorInfo) {
    console.log('Errors have come');
    this.setState({
      hasError: true
    })
  }
  
  setSelectedId = (id) => {
    console.log(id);
    
    this.setState({
      itemId: id
    })
  };
  
  render() {
    
    if (this.state.hasError) {
      return <ErrorLoader/>;
    }
    
    const {showRandomItem, showItemsList, showItemDetails, history} = this.props;
    
    const randomItem = showRandomItem ?
      <RandomItem url={'vehicles/'}
                  itemsListArray={this.itemsListArray}
                  getItem={this.swapiService.getVehicle}/> : null;
    
    const itemsList = showItemsList ?
      <ItemsList url={'vehicles/'}
        // onItemSelected={this.setSelectedId}
                 onItemSelected={(itemId) => {
                   const newPath = `vehicles/${itemId}`;
                   history.push(newPath);
                 }}
                 getAllItems={this.swapiService.getAllVehicles}/> : null;
    
    const itemDetails = showItemDetails ?
      <ItemDetails url={'vehicles/'}
                   itemId={this.state.itemId}
                   getItemDetails={this.swapiService.getVehicle}/> : null;
    
    return (
      <section className={`${styles.pageHeight} vehicles`}>
        <div className="row my-4">
          <div className="col-12">
            {randomItem}
          </div>
          <div className="col-md-6">
            {itemsList}
          </div>
          <div className="col-md-6">
            {itemDetails}
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(VehiclesPage);
