import React from 'react';

import ErrorLoader from "../../components/errorLoader"; // import RandomItem from "../randomItem";
import ItemsList from "../../components/itemsList";
import ItemDetails from "../../components/itemDetails";
import RandomItem from "../../components/randomItem";

import './vehiclesPage.module.scss';


export default class BasicPage extends React.Component {
  
  state = {
    itemId: Math.floor(Math.random() * (19 - 3)) + 3,
    hasError: false
  };
  
  // componentWillUnmount() {
  //   this.setState(() => {
  //     return {
  //       itemId: null
  //     };
  //   });
  // }
  
  // setSelectedId = (id) => {
  //   console.log(id);
  //
  //   this.setState({
  //     itemId: id
  //   })
  // };
  
  render() {
    
    if (this.state.hasError) {
      return <ErrorLoader/>;
    }
    
    const {showRandomItem, showItemsList, showItemDetails} = this.props;
    
    const randomItem = showRandomItem ?
      <RandomItem url={'vehicles/'}
                  getItem={this.swapiService.getVehicle}/> : null;
    
    const itemsList = showItemsList ?
      <ItemsList onItemSelected={this.setSelectedId}
                 getAllItems={this.swapiService.getAllVehicles}/> : null;
    
    const itemDetails = showItemDetails ?
      <ItemDetails url={'vehicles/'}
                   itemId={this.state.itemId}
                   getItemDetails={this.swapiService.getVehicle}/> : null;
    
    return (
      <section className="vehicles">
        <div className="container">
          <div className="row my-4">
            <div className="col-12">
              {randomItem}
            </div>
            <div className="col-md-3">
              {itemsList}
            </div>
            <div className="col-md-9">
              {itemDetails}
            </div>
          </div>
        </div>
      </section>
    );
  }
}
