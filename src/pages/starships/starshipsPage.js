import React from 'react';
import {withRouter} from 'react-router-dom';

import ErrorLoader from "../../components/errorLoader"; // import RandomItem from "../randomItem";
import ItemsList from "../../components/itemsList";
import SwapiService from "../../services/swapiService";
import ItemDetails from "../../components/itemDetails";
import RandomItem from "../../components/randomItem";

import styles from "./starshipsPage.module.scss";


class StarshipsPage extends React.Component {
  
  swapiService = new SwapiService();
  
  itemsListArray = [5, 9, 10, 11, 12, 13, 15, 21, 22, 23];
  
  state = {
    itemId: Math.floor(Math.random() * (19 - 3)) + 3,
    hasError: false
  };
  
  componentWillUnmount() {
    this.setState(() => {
      return {
        itemId: null
      };
    });
  }
  
  componentDidCatch(error, errorInfo) {
    console.log('Errors have come');
    this.setState({
      hasError: true
    })
  }
  
  setSelectedId = (id) => {
    console.log(id);
    
    this.setState({
      itemId: id
    })
  };
  
  render() {
    
    if (this.state.hasError) {
      return <ErrorLoader/>;
    }
    
    const {showRandomItem, showItemsList, showItemDetails, history} = this.props;
    
    const randomItem = showRandomItem ?
      <RandomItem url={'starships/'}
                  itemsListArray={this.itemsListArray}
                  getItem={this.swapiService.getStarship}/> : null;
    
    const itemsList = showItemsList ?
      <ItemsList url={'starships/'}
        // onItemSelected={this.setSelectedId}
                 onItemSelected={(itemId) => {
                   const newPath = `starships/${itemId}`;
                   history.push(newPath);
                 }
                 }
                 getAllItems={this.swapiService.getAllStarships}/> : null;
    
    const itemDetails = showItemDetails ?
      <ItemDetails url={'starships/'}
                   itemId={this.state.itemId}
                   getItemDetails={this.swapiService.getStarship}/> : null;
    
    return (
      <section className={`${styles.pageHeight} starships`}>
        <div className="row my-4">
          <div className="col-12">
            {randomItem}
          </div>
          <div className="col-md-6">
            {itemsList}
          </div>
          <div className="col-md-6">
            {itemDetails}
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(StarshipsPage);
