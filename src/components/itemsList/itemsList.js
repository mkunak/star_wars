import React, {useEffect, useState} from "react";
// import {Route, Link} from "react-router-dom";
import uuid from 'react-uuid';
import Loader from "../loader";

import "./itemsList.scss";

export default function ItemsList({getAllItems, onItemSelected}) {
  
  const [itemsList, setItemsList] = useState(null);
  
  useEffect(() => {
    getAllItems()
      .then(itemsList => {
        console.log(itemsList);
        setItemsList(itemsList);
      });
  }, [getAllItems]);
  
  const renderItems = (arr) => {
    return arr.map((item) => {
      const {ID: id, Name: name} = item;
      return (
        <li
          key={uuid()}
          className="list-group-item py-1 h6"
          onClick={() => onItemSelected(id)}
        >
          {name}
        </li>
      );
    });
  };
  
  if (!itemsList) {
    return <Loader/>;
  }
  
  const items = renderItems(itemsList);
  
  return (
    <ul className="list-group">
      {items}
    </ul>
  );
}
