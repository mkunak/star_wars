import React from 'react';

import './loader.scss';

const Loader = () => {
  return (
    <div className="lds-css ng-scope">
      <div className="lds-double-ring">
        <div/>
        <div/>
        <div>
          <div/>
        </div>
        <div>
          <div/>
        </div>
      </div>
      <style type="text/css"/>
    </div>
  );
};

export default Loader;
