import React, {useEffect, useState} from 'react';

import Loader from '../loader';
import ErrorLoader from '../errorLoader';
import RandomItemContent from './randomItemContent';

import './randomItem.module.scss';


export default function RandomItem({url, getItem, itemsListArray}) {
  
  const [item, setItem] = useState({});
  const [loading, isLoading] = useState(true);
  const [error, isError] = useState(false);
  
  useEffect(() => {
    let interval = setInterval(updateItem, 5000);
    return () => clearInterval(interval);
  });
  
  function onError() {
    isError(true);
    isLoading(false);
  }
  
  function onItemLoaded(item) {
    setItem(item);
    isLoading(false);
  }
  
  function updateItem() {
    const id = itemsListArray[Math.floor(Math.random() * itemsListArray.length)];
    
    getItem(id)
      .then(onItemLoaded)
      .catch(onError);
  }
  
  const hasContent = !(error || loading);
  
  const errorLoader = error ? <ErrorLoader/> : null;
  const loader = loading ? <Loader/> : null;
  const content = hasContent ? <RandomItemContent url={url} item={item}/> : null;
  
  return (
    <section className="randomItem">
      {/*<div className="container p-0">*/}
      <div className="card mb-3">
        {errorLoader}
        {loader}
        {content}
      </div>
      {/*</div>*/}
    </section>
  );
}
