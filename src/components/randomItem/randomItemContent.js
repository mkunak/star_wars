import React from 'react';

import {Link} from "react-router-dom";

import DetailsList from "../detailsList";
import Utils from "../../services/utils";
import styles from './randomItem.module.scss';

/*films: Array [ "https://swapi.co/api/films/6/", "https://swapi.co/api/films/1/" ]
              residents: Array(3) [ "https://swapi.co/api/people/5/",
              "https://swapi.co/api/people/68/", "https://swapi.co/api/people/81/" ]
              url: "https://swapi.co/api/planets/2/"*/

const RandomItemContent = ({url, item}) => { // console.log(planet);
  
  const {ID: id, Name: name} = item;
  
  let randomItemList = Object.assign({}, item);
  
  for (let key in randomItemList) {
    if (
      key !== 'ID' &&
      key !== 'Name' &&
      key !== 'Model' &&
      key !== 'Gender' &&
      key !== 'Climate' &&
      key !== 'Population' &&
      key !== 'Birth Year' &&
      key !== 'Manufacturer'
    ) {
      delete randomItemList[key];
    }
  }
  
  const randomImgStyles = [
    styles.randomPersonImg,
    styles.randomPlanetImg,
    styles.randomStarshipImg,
    styles.randomVehicleImg
  ];
  
  const randomImgStyle = Utils.chooseImgStyle(url, randomImgStyles);
  
  return (
    <>
      <h3 className="card-header">Random Item</h3>
      <div className="card-body">
        <h4 className="card-title">{name.toUpperCase()}</h4>
        <h6 className="card-subtitle text-muted">ID: {id}</h6>
      </div>
      <div className="d-flex">
        <div>
          <img className={`${randomImgStyle} rounded mx-4`}
               src={`https://starwars-visualguide.com/assets/img/${url}${id}.jpg`}
               alt="Random Item"/>
        </div>
        
        <div className="card-body py-md-0">
          <h5 className="card-text">
            <span>General information:</span>
          </h5>
          
          <DetailsList details={randomItemList}/>
        
        </div>
      </div>
      
      <div className="card-footer text-muted mt-2 py-1">
        For more information visit links below, please
      </div>
      
      <div className="d-flex justify-content-center py-2">
        <Link to="#" className="card-link">Films</Link>
        <Link to="#" className="card-link">Residents</Link>
        <Link to="#" className="card-link">Another link</Link>
      </div>
    
    </>
  );
};

export default RandomItemContent;
