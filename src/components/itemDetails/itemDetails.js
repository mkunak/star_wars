import React, {Component} from 'react';

import Loader from "../loader";
import Utils from '../../services/utils';
import DetailsList from "../detailsList";
import ErrorLoader from "../errorLoader";

import styles from './itemDetails.module.scss';


class ItemDetails extends Component {
  
  state = {
    id: 1,
    item: null,
    hasError: false
  };
  
  updatePerson = () => {
    const {itemId, getItemDetails} = this.props; // console.log(this.props);
    
    if (!itemId) {
      return;
    }
    
    getItemDetails(itemId)
      .then((item) => {
        this.setState({item});
      })
      .catch((err) => {
        console.log(err);
        this.setState({hasError: true});
      });
  };
  
  componentDidMount() {
    this.updatePerson();
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.itemId !== this.props.itemId) {
      this.updatePerson();
    }
  }
  
  render() {
    
    if (this.state.hasError) {
      return <ErrorLoader/>;
    }
    
    const {item} = this.state;
    const {url} = this.props;
    
    const detailsImgStyles = [
      styles.personDetailsImg,
      styles.planetDetailsImg,
      styles.starshipDetailsImg,
      styles.vehicleDetailsImg
    ];
    
    if (!item) {
      return <Loader/>;
    }
    
    const imgStyle = Utils.chooseImgStyle(url, detailsImgStyles);
    
    const {ID: id, Name: name} = item;
    
    return (
      // <div className={`container`}>
      <div className="card">
        <h4 className="card-header">Item Details</h4>
        <div className="d-flex">
          <img
            className={imgStyle}
            src={`https://starwars-visualguide.com/assets/img/${url}${id}.jpg`}
            alt={`Person-${name}-${id}`}/>
          <div className="card-body py-2">
            <h4 className="card-text">{name.toUpperCase()}</h4>
            <h6 className="card-text text-muted">
              <span>ID: </span>
              <span className="font-weight-bold">{this.props.itemId}</span>
            </h6>
            <DetailsList details={item}/>
          </div>
        </div>
      </div>
      // </div>
    );
  }
}

export default ItemDetails;
