class Utils {
  
  static chooseImgStyle(url, stylesArray) {
    switch (url) {
      case 'characters/':
        return stylesArray[0];
      case 'planets/':
        return stylesArray[1];
      case 'starships/':
        return stylesArray[2];
      case 'vehicles/':
        return stylesArray[3];
      default:
        return;
    }
  };
  
  normalizeName(name) {
    let hasNumber = Array.from(name).find((item) => (Number(item)));
    if (hasNumber) {
      return name.toUpperCase();
    }
    
    const space = ' ';
    const minus = '-';
    const lowdash = '_';
    const arr = name.split(space).map((item) => {
      
      let hasLowdash = Array.from(name).find(() => (lowdash));
      let hasMinus = Array.from(name).find(() => (minus));
      
      if (hasMinus) {
        let array = item.split(minus).map(elem => elem.charAt(0).toUpperCase() + elem.slice(1).toLowerCase());
        return array.join(minus);
      }
      
      if (hasLowdash) {
        let array = item.split(lowdash).map(elem => elem.charAt(0).toUpperCase() + elem.slice(1).toLowerCase());
        return array.join(space);
      }
      
      return item.charAt(0).toUpperCase() + item.slice(1).toLowerCase()
    });
  
    return arr.join(space);
  }
}

export default Utils;
